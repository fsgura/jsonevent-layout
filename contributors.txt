# Contributors for the original project

- @pyr (Pierre-Yves Ritschard)
- @StFS (Stefán Freyr Stefánsson)
- @looztra (Christophe Furmaniak)
- @astrochoupe
- @bfritz (Brad Fritz)
- @vrivellino (Vincent R.)

# Contributors for the forked project
- @fsgura@psl.com.co (Fabrizio Sgura)
